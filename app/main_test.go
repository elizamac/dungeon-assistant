package main

import "testing"

func TestMain(t *testing.T) {

	// Add test player
	new_player := Player{
		Name: "Eliza",
	}
	new_player_id := newPlayer(new_player)
	debugLogger.Println("New player name:", getValue("players", "name", new_player_id))

	// Add test character
	new_character := Character{
		Name:                        "Aster",
		Player_id:                   new_player_id,
		Age:                         167,
		Height_cm:                   158,
		Saving_throws_proficiencies: 3,
		Personality_traits:          "I interpret every event as part of a larger pattern I just haven't worked out yet. (Puzzle, Star) [Ruined]",
	}
	new_character_id := newCharacter(new_character)
	debugLogger.Println("New Character name:", getValue("characters", "name", new_character_id))

	// Test height conversion
	myHeight_cm := 158
	myHeight_ft := CmToFt(myHeight_cm)
	debugLogger.Println("Height in CM:", myHeight_cm, "; Height in FT", myHeight_ft)

	myHeight_ft = HeightFT{
		feet:   5,
		inches: 9,
	}
	myHeight_cm = FtToCm(myHeight_ft)
	debugLogger.Println("Height in FT:", myHeight_ft, "; Height in CM", myHeight_cm)
}
