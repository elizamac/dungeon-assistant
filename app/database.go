package main

import (
	"database/sql"
	"fmt"
	"os"
	"reflect"
	"strconv"

	"github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

type HeightFT struct {
	feet   int
	inches int
}

func CmToFt(height_cm int) (height_ft HeightFT) {
	height_ft.feet = int(float32(height_cm) / 30.48)
	height_ft.inches = int((float32(height_cm) - (float32(height_ft.feet) * 30.48)) / 2.54)
	return height_ft
}

func FtToCm(height_ft HeightFT) (height_cm int) {
	return int((float32(height_ft.feet) * 30.48) + (float32(height_ft.inches) * 2.54))
}

func loadEnv() (database databaseDetails) {
	// Load the environment variables
	debugLogger.Println("Loading environment variables.")
	database.User = os.Getenv("DB_USER")
	database.Pass = os.Getenv("DB_PASS_ROOT")
	database.Name = os.Getenv("DB_NAME")
	database.Host = os.Getenv("DB_HOST")
	database.Port = os.Getenv("DB_PORT")

	// Load .env file variables
	debugLogger.Println("Loading .env file variables if environment variables are not set.")
	debugLogger.Println("Opening .env file.")
	if err := godotenv.Load(); err != nil {
		errorLogger.Println("No .env file found")
	} else {
		var exists bool

		/* If there aren't any environment variables
		try to load them from the .env file */
		if len(database.User) == 0 {
			debugLogger.Println("Didn't find DB_USER environment variable. Trying to load it from .env file.")
			database.User, exists = os.LookupEnv("DB_USER")
			if !exists {
				/* If there are no values in the .env file then
				use the default values if possible*/
				infoLogger.Println("Didn't find DB_USER environment variable. Using default value of 'root'")
			} else {
				debugLogger.Println("DB_USER environment variable set to", database.User)
			}
			exists = false
		}

		if len(database.Pass) == 0 {
			debugLogger.Println("Didn't find DB_PASS environment variable. Trying to load it from .env file.")
			database.Pass, exists = os.LookupEnv("DB_PASS_ROOT")
			if !exists {
				// Password is required!
				fatalLogger.Fatalf("Didn't find DB_PASS environment variable! Please provide the root Password")
			} else {
				debugLogger.Println("DB_PASS environment variable loaded")
			}
			exists = false
		}

		if len(database.Host) == 0 {
			debugLogger.Println("Didn't find DB_HOST environment variable. Trying to load it from .env file.")
			database.Host, exists = os.LookupEnv("DB_HOST")
			if !exists {
				infoLogger.Println("No value for DB_HOST environment variable. Using default value: 'localHost'")
			} else {
				debugLogger.Println("DB_HOST environment variable set to", database.Host)
			}
			exists = false
		}

		if len(database.Port) == 0 {
			debugLogger.Println("Didn't find DB_PORT environment variable. Trying to load it from .env file.")
			database.Port, exists = os.LookupEnv("DB_PORT")
			if !exists {
				infoLogger.Println("No value for DB_PORT environment variable. Using default value: '3306'")
			} else {
				debugLogger.Println("DB_PORT environment variable set to", database.Port)
			}
			exists = false
		}

		if len(database.Name) == 0 {
			debugLogger.Println("Didn't find DB_NAME environment variable. Trying to load it from .env file.")
			database.Name, exists = os.LookupEnv("DB_NAME")
			if !exists {
				infoLogger.Println("No value for DB_NAME environment variable. Using default value: 'dungeon_assistant'")
			} else {
				debugLogger.Println("DB_NAME environment variable set to", database.Name)
			}
			exists = false
		}
	}

	if len(database.User) == 0 {
		database.User = "root"
		debugLogger.Println("No value for DB_USER environment variable. Using default value of 'root'")
	}

	if len(database.Name) == 0 {
		database.Name = "dungeon_assistant"
		debugLogger.Println("No value for DB_NAME environment variable. Using default value of 'dungeon_assistant'")
	}

	if len(database.Host) == 0 {
		database.Host = "localHost"
		debugLogger.Println("No value for DB_HOST environment variable. Using default value of 'localHost'")
	}

	if len(database.Port) == 0 {
		database.Port = "3306"
		debugLogger.Println("No value for DB_PORT environment variable. Using default value of '3306'")
	}

	return database
}

func checkDatabaseConnection(database_User string, database_Password string, database_Host string, database_Port string) (err error) {
	// open connection to sql server
	debugLogger.Println("Attempting to connect to database server...")
	db, err := openDatabaseConnection(database_User, database_Password, database_Host, database_Port, "")
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to open connection to database server:", err)
	}
	defer db.Close()
	err = db.Ping()
	return err
}

func openDatabaseConnection(database_User string, database_Password string, database_Host string, database_Port string, database_Name string) (db *sql.DB, err error) {
	// open connection to sql server
	debugLogger.Println("Attempting to connect to database server...")
	db, err = sql.Open("mysql", database_User+":"+database_Password+"@tcp("+database_Host+":"+database_Port+")/"+database_Name)
	return db, err
}

func initDatabase(database_User string, database_Password string, database_Host string, database_Port string, database_Name string) {
	debugLogger.Println("Initializing database.")
	// open connection to sql server
	db, err := openDatabaseConnection(database_User, database_Password, database_Host, database_Port, "")
	if err != nil {
		fatalLogger.Fatal("Unable to open connection to database server.", err)
	}
	defer db.Close()
	debugLogger.Println("Done.")

	// create new database
	debugLogger.Println("Creating database...")
	_, err = db.Exec("CREATE DATABASE " + database_Name)
	if err != nil {
		if mysqlErr, ok := err.(*mysql.MySQLError); ok && mysqlErr.Number == 1007 {
			errorLogger.Println("Database already exists:", mysqlErr.Message)
		} else {
			fatalLogger.Fatal("Unable to create database. ", err)
		}
	}
	debugLogger.Println("Done.")

	// use new database
	debugLogger.Println("Selecting newly created database...")
	_, err = db.Exec("USE " + database_Name)
	if err != nil {
		fatalLogger.Fatal("Unable to select database.", err)
	}
	debugLogger.Println("Done.")

	// initialize players table
	debugLogger.Println("Creating players database table...")
	_, err = db.Exec("CREATE TABLE players ( id INT NOT NULL AUTO_INCREMENT, name VARCHAR(255) NOT NULL, PRIMARY KEY (id) )")
	if err != nil {
		if mysqlErr, ok := err.(*mysql.MySQLError); ok && mysqlErr.Number == 1050 {
			errorLogger.Println("Table already exists:", mysqlErr.Message)
		} else {
			fatalLogger.Fatal("Unable to create players database table.", err)
		}
	}
	debugLogger.Println("Done.")

	// initialize characters table
	debugLogger.Println("Creating characters database table...")
	_, err = db.Exec(`CREATE TABLE characters (
    id INT NOT NULL AUTO_INCREMENT,
    player_id INT NOT NULL,
    name VARCHAR(255) NOT NULL,
		race VARCHAR(40),
    age INT,
    height_cm SMALLINT,
    height_ft_feet SMALLINT,
    height_ft_inches SMALLINT,
    weight SMALLINT,
    weight_unit VARCHAR(20),
    eyes VARCHAR(50),
    skin VARCHAR(50),
    hair VARCHAR(50),
    background VARCHAR(50),
    alignment VARCHAR(30),
    armor_class SMALLINT DEFAULT 0,
    initiative SMALLINT DEFAULT 0,
    speed SMALLINT,
    strength SMALLINT DEFAULT 10,
    dexterity SMALLINT DEFAULT 10,
    constitution SMALLINT DEFAULT 10,
    intelligence SMALLINT DEFAULT 10,
    wisdom SMALLINT DEFAULT 10,
    charisma SMALLINT DEFAULT 10,
    proficiency_bonus SMALLINT DEFAULT 0,
    saving_throws_proficiencies SMALLINT DEFAULT 0,
    skills_proficiencies SMALLINT DEFAULT 0,
    max_health SMALLINT DEFAULT 0,
    current_health SMALLINT DEFAULT 0,
		money_cp INT DEFAULT 0,
		money_sp INT DEFAULT 0,
		money_ep INT DEFAULT 0,
		money_gp INT DEFAULT 0,
		money_pp INT DEFAULT 0,
		backstory MEDIUMTEXT,
		personality_traits MEDIUMTEXT,
		idealS MEDIUMTEXT,
		bonds MEDIUMTEXT,
		flaws MEDIUMTEXT,
		feats_traits MEDIUMTEXT,
		proficiency_languages MEDIUMTEXT,
    PRIMARY KEY (id),
    FOREIGN KEY (player_id) REFERENCES players(id) )`)
	if err != nil {
		if mysqlErr, ok := err.(*mysql.MySQLError); ok && mysqlErr.Number == 1050 {
			errorLogger.Println("Table already exists:", mysqlErr.Message)
		} else {
			fatalLogger.Fatal("Unable to create characters database table.", err)
		}
	}
	debugLogger.Println("Done.")
}

func setValue(table string, column string, value string, id int) {
	debugLogger.Println("Setting value of", table, "table, column", column, "to", value, "for id", id)

	// Connect to database
	debugLogger.Println("Connecting to database...")
	db, err := openDatabaseConnection(dungeon_db.User, dungeon_db.Pass, dungeon_db.Host, dungeon_db.Port, dungeon_db.Name)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to open connection to database server. Skipping operation.", err)
	}
	debugLogger.Println("Done.")
	defer db.Close()

	// Update field
	// Handle height differently
	if column != "Height_cm" && column != "Height_ft_feet" && column != "Height_ft_inches" {
		debugLogger.Println("Updating field (" + column + ")")
		_, err = db.Exec("UPDATE "+table+" SET "+column+" = ? WHERE id = ?", value, id)
		if err != nil {
			defer recoverPanic()
			errorLogger.Panic("Unable to update field. Skipping operation.", err)
		}
	}

	// For height in CM add height in Feet and Inches
	if column == "Height_cm" {
		debugLogger.Println("Updating height field (height_cm)")

		// Set height in CM
		_, err = db.Exec("UPDATE "+table+" SET "+column+" = ? WHERE id = ?", value, id)
		if err != nil {
			defer recoverPanic()
			errorLogger.Panic("Unable to update field. Skipping operation.", err)
		}

		debugLogger.Println("Converting height from CM to FT.")
		height_cm, err := strconv.Atoi(value)
		if err != nil {
			defer recoverPanic()
			debugLogger.Panic("Conversion from CM to Feet failed:", err)
		} else {
			height_ft := CmToFt(height_cm)
			// Update feet value
			_, err = db.Exec("UPDATE "+table+" SET height_ft_feet = ? WHERE id = ?", height_ft.feet, id)
			if err != nil {
				defer recoverPanic()
				errorLogger.Panic("Unable to update field. Skipping operation.", err)
			}
			// Update inches value
			_, err = db.Exec("UPDATE "+table+" SET height_ft_inches = ? WHERE id = ?", height_ft.inches, id)
			if err != nil {
				defer recoverPanic()
				errorLogger.Panic("Unable to update field. Skipping operation.", err)
			}
		}
	}

	// For height in Feet (feet) add height in CM
	if column == "Height_ft_feet" {
		debugLogger.Println("Updating height field (height_ft_feet)")

		// Set height in FT (feet)
		_, err = db.Exec("UPDATE "+table+" SET "+column+" = ? WHERE id = ?", value, id)
		if err != nil {
			defer recoverPanic()
			errorLogger.Panic("Unable to update field. Skipping operation.", err)
		}

		debugLogger.Println("Converting height from FT to CM.")
		var height_ft HeightFT
		// Get height in feet (inches)
		height_ft.inches, err = strconv.Atoi(getValue(table, "height_ft_inches", id))
		if err != nil {
			defer recoverPanic()
			errorLogger.Panic("Failed to get height in inches.")
		}
		// Convert and set height in CM
		height_ft.feet, err = strconv.Atoi(value)
		if err == nil {
			height_cm := FtToCm(height_ft)
			// Update CM value
			_, err = db.Exec("UPDATE "+table+" SET height_cm = ? WHERE id = ?", height_cm, id)
			if err != nil {
				defer recoverPanic()
				errorLogger.Panic("Unable to update field. Skipping operation.", err)
			}
		}
	}

	// For height in Feet (inches) add height in CM
	if column == "Height_ft_inches" {
		debugLogger.Println("Updating height field (height_ft_inches)")

		// Set height in FT (inches)
		_, err = db.Exec("UPDATE "+table+" SET "+column+" = ? WHERE id = ?", value, id)
		if err != nil {
			defer recoverPanic()
			errorLogger.Panic("Unable to update field. Skipping operation.", err)
		}

		debugLogger.Println("Converting height from FT to CM.")
		var height_ft HeightFT
		// Get height in feet (feet)
		height_ft.feet, err = strconv.Atoi(getValue(table, "height_ft_feet", id))
		if err != nil {
			defer recoverPanic()
			errorLogger.Panic("Failed to get height in feet.")
		}
		// Convert and set height in CM
		height_ft.inches, err = strconv.Atoi(value)
		if err == nil {
			height_cm := FtToCm(height_ft)
			// Update CM value
			_, err = db.Exec("UPDATE "+table+" SET height_cm = ? WHERE id = ?", height_cm, id)
			if err != nil {
				defer recoverPanic()
				errorLogger.Panic("Unable to update field. Skipping operation.", err)
			}
		}
	}
	debugLogger.Println("Done.")
}

func getValue(table string, column string, id int) (value string) {
	debugLogger.Println("Getting value of", table, "table, column", column, "for id", id)

	debugLogger.Print("Connecting to database...")
	db, err := openDatabaseConnection(dungeon_db.User, dungeon_db.Pass, dungeon_db.Host, dungeon_db.Port, dungeon_db.Name)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to open connection to database server. Skipping operation.", err)
	}
	debugLogger.Println("Done.")
	defer db.Close()

	debugLogger.Print("Querying database...")
	rows, err := db.Query("SELECT "+column+" FROM "+table+" WHERE id = ?", id)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to query database. Skipping operation.", err)
	}

	debugLogger.Print("Scanning result...")
	rows.Next()
	if rows.Scan(&value) != nil {
		if rows.Scan(new(interface{})) == nil {
			value = ""
		} else {
			defer recoverPanic()
			errorLogger.Panic("Unable to scan result. Skipping operation.", err)
		}
	}
	debugLogger.Println("Done.")

	return value
}

func newPlayer(add_player Player) (id int) {
	debugLogger.Println("Creating new player with name", add_player.Name)

	// Initialize database connection
	db, err := openDatabaseConnection(dungeon_db.User, dungeon_db.Pass, dungeon_db.Host, dungeon_db.Port, dungeon_db.Name)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to open connection to database server. Skipping operation.", err)
	}
	defer db.Close()

	// Insert required data to create row
	_, err = db.Exec("INSERT INTO players (name) VALUES (?)", add_player.Name)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to insert new row in players table. Skipping operation.", err)
	}

	// Get id of newly added player
	err = db.QueryRow("SELECT LAST_INSERT_ID()").Scan(&id)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to get id of new row in players table. Skipping operation.", err)
	}

	// Add other fields
	values := reflect.ValueOf(add_player)
	types := values.Type()
	for i := 0; i < values.NumField(); i++ {
		// Only add values that don't exist yet
		if current_value := getValue("players", types.Field(i).Name, id); current_value == "" {
			setValue("players", types.Field(i).Name, values.Field(i).String(), id)
		}
	}

	return id
}

func getPlayer(id int) (player Player) {
	debugLogger.Println("Getting player with id", id)
	debugLogger.Print("Connecting to database...")
	db, err := openDatabaseConnection(dungeon_db.User, dungeon_db.Pass, dungeon_db.Host, dungeon_db.Port, dungeon_db.Name)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to open connection to database server. Skipping operation.", err)
	}
	debugLogger.Println("Done.")
	defer db.Close()

	debugLogger.Print("Querying database...")
	rows, err := db.Query("SELECT * FROM players WHERE id = ?", id)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to query database. Skipping operation.", err)
	}

	debugLogger.Print("Scanning result...")
	rows.Next()
	err = rows.Scan(&player.Id, &player.Name)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to scan result. Skipping operation.", err)
	}
	debugLogger.Println("Done.")

	return player
}

func newCharacter(add_character Character) (id int) {
	debugLogger.Println("Creating new character with name", add_character.Name, "belonging to pleyer with id", add_character.Player_id)

	// Initialize database connection
	db, err := openDatabaseConnection(dungeon_db.User, dungeon_db.Pass, dungeon_db.Host, dungeon_db.Port, dungeon_db.Name)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to open connection to database server. Skipping operation.", err)
	}

	// Insert required data to create row
	defer db.Close()
	_, err = db.Exec("INSERT INTO characters (name, player_id) VALUES (?, ?)", add_character.Name, add_character.Player_id)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to insert new row in characters table. Skipping operation.", err)
	}

	// Get id of newly added character
	err = db.QueryRow("SELECT LAST_INSERT_ID()").Scan(&id)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to get id of new row in characters table. Skipping operation.", err)
	}

	// Add other values
	values := reflect.ValueOf(add_character)
	types := values.Type()
	for i := 0; i < values.NumField(); i++ {
		// Only add values that don't exist yet
		if current_value := getValue("characters", types.Field(i).Name, id); current_value == "" {
			setValue("characters", types.Field(i).Name, fmt.Sprint(values.Field(i)), id)
			debugLogger.Println("Added", types.Field(i).Name, "=", fmt.Sprint(values.Field(i)))
		}
	}
	return id
}

func getCharacter(id int) (character Character) {
	debugLogger.Println("Getting character with id", id)

	// Connect to database
	debugLogger.Print("Connecting to database...")
	db, err := openDatabaseConnection(dungeon_db.User, dungeon_db.Pass, dungeon_db.Host, dungeon_db.Port, dungeon_db.Name)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to open connection to database server. Skipping operation.", err)
	}
	debugLogger.Println("Done.")
	defer db.Close()

	// Get character from database
	debugLogger.Print("Querying database...")
	rows, err := db.Query("SELECT * FROM characters WHERE id = ?", id)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to query database. Skipping operation.", err)
	}

	// Scanning the result
	debugLogger.Print("Scanning result...")
	rows.Next()
	var weightUnit sql.NullString
	err = rows.Scan(&character.Id, &character.Player_id, &character.Name, &character.Age, &character.Height_cm, &character.Height_ft_feet, &character.Height_ft_inches, &character.Weight, &weightUnit, &character.Eyes, &character.Skin, &character.Hair, &character.Background, &character.Alignment, &character.Armor_class, &character.Initiative, &character.Speed, &character.Strength, &character.Dexterity, &character.Constitution, &character.Intelligence, &character.Wisdom, &character.Charisma, &character.Proficiency_bonus, &character.Saving_throws_proficiencies, &character.Skills_proficiencies, &character.Max_health, &character.Current_health)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Unable to scan result. Skipping operation.", err)
	}
	if weightUnit.Valid {
		character.Weight_unit = weightUnit.String
	} else {
		character.Weight_unit = ""
	}
	debugLogger.Println("Done.")

	return character
}
