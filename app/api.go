package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"reflect"
	"strconv"

	"github.com/gorilla/mux"
)

/*
Disable Database info API

	func databaseHandler(w http.ResponseWriter, r *http.Request) {
		debugLogger.Println("API Accessed: database")
		json.NewEncoder(w).Encode(dungeon_db)
	}
*/
func singlePlayerHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	playerId, err := strconv.Atoi(vars["id"])
	debugLogger.Println("API Accessed: singlePlayer with id", playerId)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Player ID is not a number:", err)
	}
	current_player := getPlayer(playerId)
	debugLogger.Println("Accessed database.")
	json.NewEncoder(w).Encode(current_player)
}

func newPlayerHandler(w http.ResponseWriter, r *http.Request) {
	// Read request body
	request_body, _ := io.ReadAll(r.Body)
	debugLogger.Printf("Posted new player data: %+v", string(request_body))

	// Decode player data
	var new_player Player
	json.Unmarshal(request_body, &new_player)

	// Check min requirements
	if new_player.Name == "" {
		defer recoverPanic()
		errorLogger.Panic("Unable to create new player: name field is empty. Example:\n{\n\t\"name\":\"<new player name>\"\n}")
	}

	// Create new player
	new_id := newPlayer(new_player)
	new_player = getPlayer(new_id)
	json.NewEncoder(w).Encode(new_player)
}

func singleCharacterHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	characterId, err := strconv.Atoi(vars["id"])
	debugLogger.Println("API Accessed: singleCharacter with id", characterId)
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Character ID is not a number:", err)
	}
	current_character := getCharacter(characterId)
	debugLogger.Println("Accessed database.")
	json.NewEncoder(w).Encode(current_character)
}

func newCharacterHandler(w http.ResponseWriter, r *http.Request) {
	request_body, _ := io.ReadAll(r.Body)
	debugLogger.Printf("Posted new character data: %+v", string(request_body))

	// Decode character data
	var new_character Character
	json.Unmarshal(request_body, &new_character)

	// Check min requirements
	if new_character.Name == "" || new_character.Player_id == 0 {
		defer recoverPanic()
		errorLogger.Panic("Unable to create new character: name field is empty. Example:\n{\n\t\"name\":\"<new player name>\",\n\t\"player_id\":\"<owner player id>\"\n}")
	}

	// Create new Character
	new_id := newCharacter(new_character)
	new_character = getCharacter(new_id)
	json.NewEncoder(w).Encode(new_character)
}

func getValueHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	// Get table
	table := vars["table"]

	// Get id
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Invalid id:", err)
	}

	// Get column
	column := vars["column"]

	debugLogger.Printf("API Accessed: GET %s/%d/%s", table, id, column)

	// Read value from database
	value := getValue(table, column, id)
	debugLogger.Println("Got value", value)
	json.NewEncoder(w).Encode(value)
}

func setValueHandler(w http.ResponseWriter, r *http.Request) {
	// Get request body
	request_body, _ := io.ReadAll(r.Body)
	debugLogger.Printf("Posted data: %+v", string(request_body))

	vars := mux.Vars(r)

	//Get table
	table := vars["table"]

	// Get id
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		defer recoverPanic()
		errorLogger.Panic("Invalid id:", err)
	}

	debugLogger.Printf("API Accessed: POST %+v to %s/%d", string(request_body), table, id)

	if table == "players" {
		// Decode data into Player variable
		var data Player
		json.Unmarshal(request_body, &data)

		// Set values
		values := reflect.ValueOf(data)
		types := values.Type()
		for i := 0; i < values.NumField(); i++ {
			setValue(table, types.Field(i).Name, values.Field(i).String(), id)
		}
	} else if table == "characters" {
		// Decode data into Character variable
		var data Character
		json.Unmarshal(request_body, &data)

		// Set values
		values := reflect.ValueOf(data)
		types := values.Type()
		for i := 0; i < values.NumField(); i++ {
			if fmt.Sprint(values.Field(i)) == "" || fmt.Sprint(values.Field(i)) == "0" {
				debugLogger.Println("Empty value of", types.Field(i).Name, "=", fmt.Sprint(values.Field(i)))
			} else {
				debugLogger.Println("Setting value of", types.Field(i), "to", fmt.Sprint(values.Field(i)), "of type", reflect.TypeOf(fmt.Sprint(values.Field(i))))
				setValue(table, types.Field(i).Name, fmt.Sprint(values.Field(i)), id)
			}
		}
	} else {
		defer recoverPanic()
		errorLogger.Panic("Invalid table name:", table)
	}

}

func handleRequests() {
	webRouter := mux.NewRouter().StrictSlash(true)

	//webRouter.HandleFunc("/database", databaseHandler)
	webRouter.HandleFunc("/players/{id}", singlePlayerHandler).Methods("GET")
	webRouter.HandleFunc("/characters/{id}", singleCharacterHandler).Methods("GET")
	webRouter.HandleFunc("/{table}/{id}/{column}", getValueHandler).Methods("GET")
	webRouter.HandleFunc("/players", newPlayerHandler).Methods("POST")
	webRouter.HandleFunc("/characters", newCharacterHandler).Methods("POST")
	webRouter.HandleFunc("/{table}/{id}", setValueHandler).Methods("POST")

	log.Fatal(http.ListenAndServe(":3000", webRouter))
}
