package main

import (
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"time"

	"github.com/joho/godotenv"
)

// create structure for players
type Player struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

// create structure for characters
type Character struct {
	Id                          int    `json:"id"`
	Player_id                   int    `json:"player_id"`
	Name                        string `json:"name"`
	Age                         int    `json:"age"`
	Height_cm                   int    `json:"height_cm"`
	Height_ft_feet              int    `json:"height_ft_feet"`
	Height_ft_inches            int    `json:"height_ft_inches"`
	Weight                      int    `json:"weight"`
	Weight_unit                 string `json:"weight_unit"`
	Eyes                        string `json:"eyes"`
	Skin                        string `json:"skin"`
	Hair                        string `json:"hair"`
	Background                  string `json:"background"`
	Alignment                   string `json:"alignment"`
	Armor_class                 int    `json:"armor_class"`
	Initiative                  int    `json:"initiative"`
	Speed                       int    `json:"speed"`
	Strength                    int    `json:"strength"`
	Dexterity                   int    `json:"dexterity"`
	Constitution                int    `json:"constitution"`
	Intelligence                int    `json:"intelligence"`
	Wisdom                      int    `json:"wisdom"`
	Charisma                    int    `json:"charisma"`
	Proficiency_bonus           int    `json:"proficiency_bonus"`
	Saving_throws_proficiencies int    `json:"saving_throws_proficiencies"`
	Skills_proficiencies        int    `json:"skills_proficiencies"`
	Max_health                  int    `json:"max_health"`
	Current_health              int    `json:"current_health"`
	Money_cp                    int    `json:"money_cp"`
	Money_sp                    int    `json:"money_sp"`
	Money_ep                    int    `json:"money_ep"`
	Money_gp                    int    `json:"money_gp"`
	Money_pp                    int    `json:"money_pp"`
	Backstory                   string `json:"backstory"`
	Personality_traits          string `json:"personality_traits"`
	Ideals                      string `json:"ideals"`
	Bonds                       string `json:"bonds"`
	Flaws                       string `json:"flaws"`
	Feats_traits                string `json:"feats_traits"`
	Proficiency_languages       string `json:"proficiency_languages"`
}

// create structure for database
type databaseDetails struct {
	User string `json:"User"`
	Pass string `json:"-"`
	Host string `json:"Host"`
	Port string `json:"Port"`
	Name string `json:"Name"`
}

var dungeon_db databaseDetails

// create loggers
// ?var warnLogger *log.Logger
var debugLogger *log.Logger
var infoLogger *log.Logger
var errorLogger *log.Logger
var fatalLogger *log.Logger

func recoverPanic() {
	if r := recover(); r != nil {
		debugLogger.Println("Recovered in recoverPanic", r)
	}
}

func init() {
	// Load user definer log level
	logLevelString := os.Getenv("LOG_LEVEL")

	// Load .env file variables
	log.Println("Loading .env file variables if environment variables are not set.")
	log.Println("Opening .env file.")
	if err := godotenv.Load(); err != nil {
		log.Println("No .env file found")
	} else {
		var exists bool

		/* If there is no log level defined
		try to load from the .env file */
		if len(logLevelString) == 0 {
			log.Println("Didn't find LOG_LEVEL environment variable. Trying to load it from .env file.")
			logLevelString, exists = os.LookupEnv("LOG_LEVEL")
			if !exists {
				/* If there is no value in the .env file either
				then use the default of 2 (only fatal and error)*/
				log.Println("Didn't find LOG_LEVEL in .env file. Using default value of '2'")
			} else {
				log.Println("LOG_LEVEL environment variable set to", logLevelString)
			}
		}
	}

	if len(logLevelString) == 0 {
		logLevelString = "2"
		log.Println("No value for LOG_LEVEL environment variable. Using default value of '2'")
	}
	// Convert string to integer
	logLevel, err := strconv.Atoi(logLevelString)
	if err != nil {
		logLevel = 2
		log.Panicln("Invalid value of LOG_LEVEL ("+logLevelString+"). Using default value of '2'.", err)
		recoverPanic()
	}

	// Get current date and time
	currentTime := time.Now().Format("2006-01-02_15-04-05")
	// Create output files
	logFile := new(os.File)
	if runtime.GOOS == "windows" {
		// Create log folder and file
		currentPath, err := filepath.Abs(".\\")
		if err != nil {
			log.Fatalln("FATAL: Failed to get current path", err)
		}
		err = os.Mkdir(currentPath+"\\logs", os.ModeDir)
		if err != nil {
			log.Fatalln("FATAL: Failed to create log directory:", err)
		}
		logFile, err = os.Create(currentPath + "\\logs\\" + currentTime + ".log")
		if err != nil {
			log.Fatalln("FATAL: Failed to create log file:", err)
		}
	} else {
		err := os.MkdirAll("/var/log/dungeon-assistant", os.ModeDir)
		if err != nil {
			log.Fatalln("FATAL: Failed to create log directory:", err)
		}
		logFile, err = os.Create("/var/log/dungeon-assistant/" + currentTime + ".log")
		if err != nil {
			log.Fatalln("FATAL: Failed to create log file:", err)
		}
	}

	logWriter := io.Writer(logFile)

	// Define output multiplexers
	toBoth := io.MultiWriter(logWriter, os.Stdout)

	/*
		========LOG LEVELS========
			0 - NONE
			1 - FATAL
			2 - FATAL, ERROR
			3 - FATAL, ERROR, INFO
			4 - ALL
		====to print on screen====

		========LOG LEVELS========
			0 - FATAL
			1 - FATAL, ERROR
			2 - FATAL, ERROR, INFO
		3,4 - ALL
		=====to print in file=====
	*/

	// Setup loggers
	log_flags := log.Ldate | log.Ltime | log.Lshortfile
	//warnLogger = log.New(os.Stdout, "WARN: ", log_flags)

	if logLevel > 3 {
		debugLogger = log.New(toBoth, "DEBUG: ", log_flags)
	} else if logLevel > 2 {
		debugLogger = log.New(logWriter, "DEBUG: ", log_flags)
	} else {
		debugLogger = log.New(io.Discard, "DEBUG: ", log_flags)
	}

	if logLevel > 2 {
		infoLogger = log.New(toBoth, "INFO: ", log_flags)
	} else if logLevel > 1 {
		infoLogger = log.New(logWriter, "INFO: ", log_flags)
	} else {
		infoLogger = log.New(io.Discard, "INFO: ", log_flags)
	}

	if logLevel > 1 {
		errorLogger = log.New(toBoth, "ERROR: ", log_flags)
	} else if logLevel > 0 {
		errorLogger = log.New(logWriter, "ERROR: ", log_flags)
	} else {
		errorLogger = log.New(io.Discard, "ERROR: ", log_flags)
	}

	if logLevel > 0 {
		fatalLogger = log.New(toBoth, "FATAL: ", log_flags)
	} else {
		fatalLogger = log.New(logWriter, "FATAL: ", log_flags)
	}

	// Load env variables
	dungeon_db = loadEnv()

	debugLogger.Println("Testing connection to", dungeon_db.Host)
	// Test database connection
	tries := 30
	for i := 0; i < tries; i++ {
		err := checkDatabaseConnection(dungeon_db.User, dungeon_db.Pass, dungeon_db.Host, dungeon_db.Port)
		if err == nil {
			debugLogger.Println("Connection to", dungeon_db.Host, "successful.")
			break
		} else {
			if i == tries-1 {
				fatalLogger.Fatal("Failed to connect to", dungeon_db.Host, "[Attempt", i+1, "/", tries, "]. Giving up.")
			}
			infoLogger.Println("Failed to connect to", dungeon_db.Host, "[Attempt", i+1, "/", tries, "]. Retrying in 3 seconds...")
		}
		time.Sleep(3 * time.Second)
	}

	// Setup database
	initDatabase(dungeon_db.User, dungeon_db.Pass, dungeon_db.Host, dungeon_db.Port, dungeon_db.Name)
}

func main() {
	debugLogger.Println("Application started")
	handleRequests()
}
